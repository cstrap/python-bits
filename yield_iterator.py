"""
Generators + itertools
"""

from itertools import islice

def fact(n):
    for i in islice(xrange(n + 1), 1, None):
        yield i


print reduce(lambda x, y: x * y, [x for x in fact(1001)])
