"""
Generators
"""

def fact(n):
    for i in xrange(1, n + 1):
        yield i


print reduce(lambda x, y: x * y, [x for x in fact(1001)])

"""
Equivalent to write, but less pythonic :-P

gen = fact(1001)
res = 1
for x in gen:
    res *= x 

print res
"""