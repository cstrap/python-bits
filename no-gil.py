"""
Cheat the GIL
"""


def fact(n, acc=1):
    while True:
        if n == 1:
            return acc
        n, acc = n - 1, acc * n


print fact(3)

print fact(1001)